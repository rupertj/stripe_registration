<?php

namespace Drupal\stripe_registration\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\stripe_registration\StripeRegistrationService;
use Drupal\Core\Entity\EntityManager;

/**
 * Class UserSubscriptionsController.
 *
 * @package Drupal\stripe_registration\Controller
 */
class UserSubscriptionsController extends ControllerBase {

  /**
   * Drupal\stripe_registration\StripeRegistrationService definition.
   *
   * @var \Drupal\stripe_registration\StripeRegistrationService
   */
  protected $stripeApi;
  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(StripeRegistrationService $stripe_api, EntityManager $entity_manager) {
    $this->stripeApi = $stripe_api;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stripe_registration.stripe_api'),
      $container->get('entity.manager')
    );
  }

  /**
   * Getusersubscriptions.
   *
   * @return string
   *   Return Hello string.
   */
  public function viewAll(UserInterface $user) {

    if ($user->stripe_customer_id->value && $user_subscriptions = $this->stripeApi->loadRemoteSubscriptionByUser($user)) {

      $output['subscriptions'] = array(
        '#type' => 'table',
        '#header' => array($this->t('Plan'), $this->t('Status'), $this->t('Current Period'), $this->t('Will renew'), $this->t('Operations')),
        '#empty' => $this->t('No subscriptions.'),
        '#attributes' => ['class' => ['stripe-subscriptions']]
      );

      /** @var Subscription $remote_subscription */
      foreach ($user_subscriptions->data as $remote_subscription) {
        $local_subscription = $this->stripeApi->loadLocalSubscription(['subscription_id' => $remote_subscription->id]);
        // Attempt to create the subscription locally.
        if (!$local_subscription) {
          $local_subscription = $this->stripeApi->createLocalSubscription($remote_subscription);
        }

        // Show local subscription, if active.
        if ($local_subscription && empty($remote_subscription->ended_at)) {

          $output['subscriptions'][$remote_subscription->id]['plan'] = [
            '#plain_text' => $remote_subscription->plan->name,
          ];
          $output['subscriptions'][$remote_subscription->id]['status'] = [
            '#plain_text' => $remote_subscription->status,
          ];
          $output['subscriptions'][$remote_subscription->id]['period'] = [
            '#plain_text' => DrupalDateTime::createFromTimestamp($remote_subscription->current_period_start)->format('F d, Y') . ' - ' . DrupalDateTime::createFromTimestamp($remote_subscription->current_period_end)->format('F d, Y'),
          ];
          $output['subscriptions'][$remote_subscription->id]['renew'] = [
            '#plain_text' => $remote_subscription->cancel_at_period_end ? $this->t('No') : $this->t('Yes'),
          ];

          $output['subscriptions'][$remote_subscription->id]['operations'] = [];

          // Cancel button.
          if (!$remote_subscription->cancel_at_period_end) {
            $output['subscriptions'][$remote_subscription->id]['operations']['data'] = [
              '#type' => 'operations',
              '#links' => [
                'delete' => [
                  'title' => t('Cancel'),
                  'url' => Url::fromRoute('stripe_registration.stripe-subscription.cancel', ['remote_id' => $remote_subscription->id]),
                ],
              ],
            ];
          }
          // Re-activate button.
          elseif (REQUEST_TIME < $remote_subscription->current_period_end) {
            $output['subscriptions'][$remote_subscription->id]['operations']['reactivate'] = [
              '#type' => 'operations',
              '#links' => [
                'delete' => [
                  'title' => t('Re-activate'),
                  'url' => Url::fromRoute('stripe_registration.stripe-subscription.reactivate', ['remote_id' => $remote_subscription->id]),
                ],
              ],
            ];
          }
        }
      }
      return $output;
    }
    else {
      return $this->redirect('stripe_registration.subscribe', ['user' => $this->currentUser()->id()]);
    }
  }

  /**
   * SubscribeForm.
   *
   * @return array
   *   Return SubscribeForm.
   */
  public function subscribeForm() {
    $form = $this->formBuilder()->getForm('Drupal\stripe_registration\Form\StripeSubscribeForm');

    return $form;
  }

  /**
   *
   */
  public function cancelSubscription() {
    $remote_id = \Drupal::request()->get('remote_id');
    $this->stripeApi->cancelRemoteSubscription($remote_id);
    $this->stripeApi->syncRemoteSubscriptionToLocal($remote_id);

    return $this->redirect("stripe_registration.user.subscriptions.viewall", [
      'user' => $this->currentUser()->id(),
    ]);
  }


  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessCancelSubscription(AccountInterface $account) {
    $remote_id = \Drupal::request()->get('remote_id');

    return AccessResult::allowedIf($account->hasPermission('administer stripe subscriptions') ||
      ($account->hasPermission('manage own stripe subscriptions') && $this->stripeApi->userHasStripeSubscription($account, $remote_id)));
  }

  /**
   *
   */
  public function reactivateSubscription() {
    $remote_id = \Drupal::request()->get('remote_id');

    $this->stripeApi->reactivateRemoteSubscription($remote_id);
    $this->stripeApi->syncRemoteSubscriptionToLocal($remote_id);

    return $this->redirect("stripe_registration.user.subscriptions.viewall", [
      'user' => $this->currentUser()->id(),
    ]);
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessReactivateSubscription(AccountInterface $account) {
    $remote_id = \Drupal::request()->get('remote_id');

    return AccessResult::allowedIf($account->hasPermission('administer stripe subscriptions') ||
      ($account->hasPermission('manage own stripe subscriptions') && $this->stripeApi->userHasStripeSubscription($account, $remote_id)));
  }

  /**
   * Shows the SCA / 3D Secure prompt.
   * @return mixed
   */
  public function viewSCA() {

    if (empty($_SESSION['stripe_registration']['payment_intent_client_secret'])) {
      // We can't do anything if this var isn't set.
      // Redirect back to the subscribe screen.
      return $this->redirect('stripe_registration.subscribe');
    }

    $stripe_api = \Drupal::service('stripe_api.stripe_api');

    $rtn['#attached']['library'][] = 'stripe_registration/stripe.stripejs';
    $rtn['#attached']['library'][] = 'stripe_registration/checkout';

    $rtn['#attached']['drupalSettings']['stripe_registration'] = [
      'publishable_key' => $stripe_api->getPubKey(),
      'payment_intent_client_secret' => $_SESSION['stripe_registration']['payment_intent_client_secret'],
      'subscribe_path' => Url::fromRoute('stripe_registration.subscribe')->toString(),
      'sca_complete_path' => Url::fromRoute('stripe_registration.sca_complete')->toString(),
    ];

    return $rtn;
  }

  /**
   * Completes the SCA / 3D Secure process.
   */
  public function completeSCA() {

    // Retrieve the subscription we're trying to buy to check its status.
    $subscription = \Stripe\Subscription::retrieve($_SESSION['stripe_registration']['subscription_id']);

    if ($subscription->status === \Stripe\Subscription::STATUS_ACTIVE) {

      // If the status indicates that SCA passed, create the local sub.
      $this->stripeApi->createLocalSubscription($subscription);

      \Drupal::messenger()->addMessage(t("You have successfully subscribed to the @plan_name plan.", ['@plan_name' => $subscription->plan->id]), 'status');

      unset($_SESSION['stripe_registration']);

      return $this->redirect('stripe_registration.my_subscriptions');
    }

    // @todo: Handle error case better.
    \Drupal::messenger()->addMessage(t("You have not been subscribed to the @plan_name plan.", ['@plan_name' => $subscription->plan->id]), 'error');
    return $this->redirect('stripe_registration.my_subscriptions');

  }
}
