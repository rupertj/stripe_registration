/**
 * @file
 */

var initialized = false;

Drupal.behaviors.stripe_registration = {
  attach: function (context, settings) {

    // Only run once.
    if (initialized) {
      return;
    }
    initialized = true;

    (function ($, Stripe, drupalSettings) {

      var stripe = Stripe(drupalSettings.stripe_registration.publishable_key);

      // First check if we're being asked to get the user to go through SCA. Otherwise take payment like normal.
      if (drupalSettings.stripe_registration.payment_intent_client_secret) {
        showSCA(drupalSettings.stripe_registration.payment_intent_client_secret);
        return;
      }

      // Create an instance of Elements.
      var elements = stripe.elements();

      // Create an instance of the card Element.
      var card = elements.create('card', {
        hidePostalCode: true,
      });

      // Add an instance of the card Element into the `card-element` <div>.
      card.mount('#card-element');

      // Handle form submission.
      var $form = $('input[value="' + drupalSettings.stripe_registration.form_id + '"]', context).parents('form');

      $form.submit(function (event) {

        // Dont run stripe processing again.
        if($form.hasClass('stripe-processed')){
          return;
        }

        // Prevent multiple clicks.
        $form.find('.form-submit').prop('disabled', true);

        stripe.createPaymentMethod({
          type: 'card',
          card: card
        }).then(stripeResponseHandler);

        // Prevent the form from being submitted. It will be submitted by stripeResponseHandler instead.
        return false;
      });

      /**
       * This function will be called by Stripe.JS once Stripe sends us a response.
       */
      function stripeResponseHandler(response) {

        if (response.error) {
          // Show the errors on the form.
          $form.find('#edit-stripe-messages').text(response.error.message);

          // Re-enable submission.
          $form.find('.form-submit').prop('disabled', false);
        }
        else {
          // Get the token ID.
          $('input[name="paymentMethodID"]').val(response.paymentMethod.id);

          // Add processed class.
          $form.addClass('stripe-processed');

          // Submit the form.
          $form.find('.form-submit').last().prop('disabled', false).click();
        }
      }

      function showSCA(paymentIntentSecret) {
        stripe.confirmCardPayment(paymentIntentSecret).then(function(result) {
          if (result.error) {
            // Display error.message in the UI, then redirect.
            alert(result.error.message);
            window.location.href = drupalSettings.stripe_registration.subscribe_path;
          }
          else {
            // The payment has succeeded. Redirect to the page that completes the subscription.
            window.location.href = drupalSettings.stripe_registration.sca_complete_path;
          }
        });
      }

    })(jQuery, Stripe, drupalSettings);
  }
};
